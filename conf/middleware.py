import json
from urllib.parse import urlparse

from django.contrib.sites.models import Site
from django.http import HttpResponseBadRequest
from sentry_sdk import capture_message

from .settings import ADMIN_URL, DEBUG, LOCAL, TESTING_MODE


class SiteDetectionMiddleware1:
    def __init__(self, get_response):
        self.get_response = get_response
        self.request = None

    def __call__(self, request):
        self.request = request
        self.request.site = None
        self.request.site_id = None
        if not self.request.META.get('HTTP_AUTHORIZATION_EVO', None) \
                and not self.request.environ['PATH_INFO'].startswith(f'/{ADMIN_URL}') \
                and not self.request.environ['PATH_INFO'].startswith(f'/swagger'):
            early_response = self.detect_site_id()
            if early_response:
                return early_response

        response = self.get_response(self.request)
        return response

    def detect_site_id(self):
        if DEBUG and LOCAL and not TESTING_MODE:
            self.request.site = Site.objects.get(pk=1)
            self.request.site_id = self.request.site.id
            return

        referer = self.request.META.get('HTTP_REFERER', None)
        custom_site = self.request.META.get('HTTP_CUSTOM_SITE', None)
        host = None

        if custom_site is not None:
            host = urlparse(custom_site).hostname
        elif referer is not None:
            host = urlparse(referer).hostname

        if self.request.environ['PATH_INFO'].startswith(f'/api/v1/payment/result'):
            host = urlparse(self.request.GET.get('referer', None)).hostname
            if host is not None:
                host = host.replace("dev.", "").replace("test.", "")

        if self.request.environ['PATH_INFO'].startswith(f'/api/v1/payment/additional_bill_result'):
            host = urlparse(self.request.GET.get('referer', None)).hostname
            if host is not None:
                host = host.replace("dev.", "").replace("test.", "")

        if self.request.environ['PATH_INFO'].startswith(f'/api/v1/sync/facebook/'):
            host = urlparse(self.request.GET.get('referer', None)).hostname
            if host is not None:
                host = host.replace("dev.", "").replace("test.", "")

        if self.request.environ['PATH_INFO'].startswith(f'/api/v1/sync/vk/'):
            host = urlparse(self.request.GET.get('referer', None)).hostname
            if host is not None:
                host = host.replace("dev.", "").replace("test.", "")

        if host is not None:
            try:
                self.request.site = Site.objects.get(domain=host)
                self.request.site_id = self.request.site.id
            except Site.DoesNotExist:
                pass

        if not self.request.site_id:
            msg = json.dumps({'detail': 'referer was not detected'})
            # capture_message(msg)  # send to sentry
            return HttpResponseBadRequest(msg, content_type='application/json')
