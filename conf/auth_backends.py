import requests
from django.contrib.sites.models import Site
from django.utils.six import text_type
from django.utils.translation import ugettext_lazy as _
from rest_framework import HTTP_HEADER_ENCODING
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import AuthenticationFailed

from main.utils import get_coffee_site, get_response_message
from users.models import User

from .settings import ADMIN_SITE_ID, GET_USER_ID_URL, TESTING_MODE


class OAuthTokenAuthentication(TokenAuthentication):
    keyword = 'Bearer'
    request_from_cashbox = False

    def get_authorization_header(self, request):
        auth = request.META.get('HTTP_AUTHORIZATION', b'')
        if not auth:
            auth = request.META.get('HTTP_AUTHORIZATION_EVO', b'')
            if auth:
                self.request_from_cashbox = True
        if isinstance(auth, text_type):
            # Work around django test client oddness
            auth = auth.encode(HTTP_HEADER_ENCODING)
        return auth

    def authenticate_credentials(self, token):
        if TESTING_MODE:
            # For testing auth header value must be "Bearer 1" (number is user id)
            user = User.objects.prefetch_related('groups').get(id=token)
            return user, token

        body = {'token': token}

        resp = requests.post(GET_USER_ID_URL, json=body, timeout=6)
        if resp.status_code != 200:
            msg = get_response_message(resp)
            raise AuthenticationFailed(msg)

        resp = resp.json()
        user = User.objects.prefetch_related('groups').get(id=resp['user_id'])
        if not user.is_active:
            raise AuthenticationFailed(_('User inactive or deleted.'))

        return user, token

    def authenticate(self, request):
        auth = self.get_authorization_header(request).split()

        if not auth or auth[0].lower() != self.keyword.lower().encode():
            return None

        if len(auth) == 1:
            msg = _('Invalid token header. No credentials provided.')
            raise AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = _('Invalid token header. Token string should not contain spaces.')
            raise AuthenticationFailed(msg)

        try:
            token = auth[1].decode()
        except UnicodeError:
            msg = _('Invalid token header. Token string should not contain invalid characters.')
            raise AuthenticationFailed(msg)

        user, token = self.authenticate_credentials(token)

        if user and not user.is_anonymous and (request.site_id == ADMIN_SITE_ID or self.request_from_cashbox):
            request.site = Site.objects.get(pk=user.site_id)
            request.site_id = user.site_id

        return user, token
