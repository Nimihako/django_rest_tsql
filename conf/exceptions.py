from rest_framework import permissions
from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    """
    Replace request.POST with drf_request.DATA so Sentry
    can log request properly.
    """
    response = exception_handler(exc, context)

    if response is None and context['request'].method not in permissions.SAFE_METHODS:
        context['request']._request.POST = context['request'].data

    return response
