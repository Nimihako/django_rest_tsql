from django.contrib import admin

from .models import User, EducationGroup


class UserAdmin(admin.ModelAdmin):
    search_fields = (
        'phone',
        'username',
    )


admin.site.register(User, UserAdmin)
admin.site.register(EducationGroup)
