from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework as df
from rest_framework.exceptions import APIException, ValidationError
from rest_framework.generics import (GenericAPIView, ListAPIView,
                                     ListCreateAPIView, RetrieveUpdateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token

from .models import *
from .serializers import *


# Create your views here.
class UserListCreateAPIView(ListCreateAPIView):
    serializer_class = UserListCreateSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        user = serializer.save(**serializer.validated_data)
        if not user.groups.first():
            user.groups.add(Group.objects.get(name='student'))
        user.set_password(user.password)
        user.save()


class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        user_data = UserListCreateSerializer(data=serializer.validated_data['user'])
        user_data.is_valid()
        return Response({
            'token': token.key,
            'user': {
                "id": user.id,
                "groups": user.groups.first().name
            },
            'email': user.email
        })


class UserProfileAPIView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = CustomerProfileSerializer
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        return self.request.user


class EducationsGroupAPIView(ListCreateAPIView):
    serializer_class = EducationsGroupCRUDSerializers
    queryset = EducationGroup.objects.all()
    pagination_class = None