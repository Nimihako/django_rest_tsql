from django.urls import path
from rest_framework.authtoken import views

from .views import UserListCreateAPIView, CustomAuthToken, UserProfileAPIView, EducationsGroupAPIView

urlpatterns = [
    path(
        'users/',
        view=UserListCreateAPIView.as_view(),
        name='list'
    ),
    path('users/auth/', CustomAuthToken.as_view()),
    path(
        'profile/',
        view=UserProfileAPIView.as_view(),
        name='profile'
    ),
    path(
        'educationgroups/',
        view=EducationsGroupAPIView.as_view(),
        name='education_group'
    )
]