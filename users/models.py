import requests
from django.contrib.auth.models import AbstractUser, Group, UserManager
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import IntegrityError, models
from django.utils.translation import gettext_lazy as _

# Create your models here.


class EducationGroup(models.Model):
    name = models.CharField(
        max_length=150,
        blank=False,
        null=False
    )


class User(AbstractUser):
    USERNAME_VALIDATOR = UnicodeUsernameValidator()
    PHONE_MAX_LENGTH = 15

    TEACHERS_GROUP_NAME = 'teachers'
    ADMINISTRATORS_GROUP_NAME = 'administrators'
    STUDENT_GROUP_NAME = 'student'

    GROUPS = {
        ADMINISTRATORS_GROUP_NAME: 'Администратор',
        TEACHERS_GROUP_NAME: 'Учитель',
        STUDENT_GROUP_NAME: 'Ученик'
    }

    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[USERNAME_VALIDATOR],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
        blank=True,
        null=True
    )
    email = models.EmailField(_('email address'), unique=True, blank=True, null=True)
    phone = models.CharField(max_length=150, blank=True, null=True)
    avatar = models.ImageField(blank=True, null=True)
    education_group = models.ForeignKey(EducationGroup, on_delete=models.CASCADE, blank=True, null=True)
    #objects = UserManager()

    def __str__(self):
        if self.username:
            return self.username
        else:
            return str(self.phone)