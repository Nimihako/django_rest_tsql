import requests
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy as _
from django.utils.duration import _get_duration_components
from rest_framework import serializers
from rest_framework.exceptions import (APIException, PermissionDenied,
                                       ValidationError)
from rest_framework.validators import UniqueValidator

from .models import User, EducationGroup


class UserListCreateSerializer(serializers.ModelSerializer):
    email = serializers.CharField(required=False, allow_null=True, validators=[UniqueValidator(queryset=User.objects.all())])
    username = serializers.CharField(required=True, validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(write_only=True)
    first_name = serializers.CharField(required=True)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'phone',
            'groups',
            'is_staff',
            'last_login',
        )
        read_only_fields = ('last_login',)

    def get_city_id(self, obj):
        request = self.context['request']
        return request.user.city

    def validate_phone(self, phone):
        if User.objects.filter(phone=phone, is_active=True).exists():
            raise ValidationError(_("Пользователь с таким телефоном уже существует"))
        return phone


class CustomerProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id',
            'first_name',
            'last_name',
            'phone',
        )


class EducationsGroupCRUDSerializers(serializers.ModelSerializer):

    class Meta:
        model = EducationGroup
        fields = (
            "id",
            "name"
        )