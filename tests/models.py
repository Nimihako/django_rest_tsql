from django.db import models
from django.contrib.auth.models import Group

from users.models import EducationGroup
from .utils import execute_sql, hash_object


class TestsSQLDatabases(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.name


class SQLTasks(models.Model):

    sql_query = models.TextField(blank=False, null=False)
    text_of_task = models.TextField(blank=False, null=False)
    databases = models.ForeignKey(TestsSQLDatabases, null=True, blank=True, on_delete=models.SET_NULL)
    white_hash = models.TextField()

    def __str__(self):
        return self.sql_query

    def save(self, *args, ** kwargs):
        if not self.white_hash:
            sql_responce = execute_sql(self.sql_query)
            self.white_hash = hash_object(sql_responce)

        super(SQLTasks, self).save(*args, **kwargs)


class SQLTest(models.Model):
    tasks = models.ManyToManyField(SQLTasks)
    name = models.CharField(max_length=255, blank=False, null=False)
    users_groups = models.ManyToManyField(EducationGroup)
    date_work_start = models.DateTimeField(auto_now_add=False)
    date_work_end = models.DateTimeField(auto_now_add=False)
