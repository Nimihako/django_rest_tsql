from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework as df
from rest_framework.exceptions import APIException, ValidationError
from rest_framework.generics import (GenericAPIView, ListAPIView,
                                     ListCreateAPIView, RetrieveUpdateAPIView,
                                     RetrieveUpdateDestroyAPIView)
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from django.db import connection, connections
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
import hashlib

from .serializers import TestsSQLDatabasesSerializer, CheckSQLQueryResponseSerializer, SQLTaskSerializer, TestsSerializer,SQLTestSerializer
from .models import TestsSQLDatabases, SQLTasks, SQLTest
from .utils import dictfetchall_from_cursor, hash_object, execute_sql


class TestsSQLDatabasesAPIView(ListCreateAPIView):
    serializer_class = TestsSQLDatabasesSerializer
    queryset = TestsSQLDatabases.objects.all()
    pagination_class = None

    def perform_create(self, serializer):
        serializer.is_valid()
        database = serializer.save(**serializer.validated_data)
        with connection.cursor() as curs:
            sql_row = "CREATE SCHEMA public.{};".format(database.name)
            curs.execute(sql_row)
            return {'status': 'ok'}


class SQLTaskDetailAPIView(ListAPIView):
    serializer_class = SQLTaskSerializer
    queryset = SQLTasks.objects.all()
    pagination_class = None

    def get_queryset(self):
        test_id = self.kwargs.get('id')
        return SQLTasks.objects.filter(sqltest__id=test_id)


class SQLTaskAPIView(ListCreateAPIView):
    serializer_class = SQLTaskSerializer
    queryset = SQLTasks.objects.all()
    pagination_class = None
    '''
    def get_queryset(self):
        test_id = self.kwargs.get('id')
        return SQLTasks.objects.filter(sqltest__id=test_id)
    '''
    def perform_create(self, serializer):
        serializer.is_valid()
        serializer.save(**serializer.validated_data)
        return {'status': 'ok'}


class CheckSQLQueryResponseAPIView(GenericAPIView):
    serializer_class = CheckSQLQueryResponseSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        number_of_sql_task = serializer.validated_data.get('number_of_sql_task', None)
        user_sql_row = serializer.validated_data.get('user_sql_row', None)
        is_true = False

        #if not number_of_sql_task:
            #return ValidationError(_('select number_of_sql_task'))

        dicter = execute_sql(user_sql_row)
        if number_of_sql_task:
            is_true = True if hash_object(dicter) == number_of_sql_task.white_hash else False

        return Response({
            'status': dicter if dicter else None,
            'is_true': is_true
        })


class UserTestAPIView(ListAPIView):
    #authentication_classes = [SessionAuthentication, BasicAuthentication, TokenAuthentication]
    serializer_class = TestsSerializer
    pagination_class = None

    def get_queryset(self):
        user_group=None

        if self.request.user.education_group:
            user_group = self.request.user.education_group
        if not user_group:
            return None
        return SQLTest.objects.filter(users_groups=user_group)


class TestsSQLAPIView(ListCreateAPIView):
    serializer_class = SQLTestSerializer
    queryset = SQLTest.objects.all()
    pagination_class = None

    def perform_create(self, serializer):
        serializer.is_valid()
        serializer.save(**serializer.validated_data)