import hashlib
import re
from collections import OrderedDict
from django.db import connection, connections
from rest_framework.exceptions import ValidationError


def dictfetchall_from_cursor(cursor):
    """

    :param cursor: opened cursor
    :return: list with dictionary value from sql query. Returns all rows from a cursor as a dict
    """
    desc = cursor.description
    return [
        dict(zip([col[0] for col in desc], row)) for row in cursor.fetchall()
    ]

def hash_object(obj: list = None):
    """

    :param obj: the list with dict values whose return after execute sql query
    :return: hash string of response sql
    """
    new_hash = ''.join((''.join(it for it in i)) for i in obj)

    return hashlib.sha256(new_hash.encode()).hexdigest()

def execute_sql(sql_query: str = None):
    """

    :param sql_query: the string with sql query
    :return: list with dictionary value from sql query. Returns all rows from a cursor as a dict
    """
    if not sql_query:
        return None
    exception_list_expressions = [
        r'UPDATE',
        r'update',
        r'DELETE',
        r'delete',
        r'CREATE',
        r'create',
        r'DROP',
        r'drop',
        r'DATABASE',
        r'database',
        r'information_schema',
        r'INFORMATION_SCHEMA',
    ]
    curs = connection.cursor()
    #sql_query = sql_query.lower()
    for exception_expr in exception_list_expressions:
        if sql_query.find(exception_expr) != -1:
            raise ValidationError({"error": "Метод запрещен"})
    try:
        curs.execute(sql_query)
        dicter = dictfetchall_from_cursor(curs)
        curs.close()
        return dicter
    except:
        curs.close()
        raise ValidationError({"error": "Ошибка запроса"})