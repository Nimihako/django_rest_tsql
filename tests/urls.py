from django.urls import path

from .views import TestsSQLDatabasesAPIView,\
    SQLTaskAPIView, CheckSQLQueryResponseAPIView, UserTestAPIView,TestsSQLAPIView,SQLTaskDetailAPIView

urlpatterns = [

    path(
        'databases/',
        view=TestsSQLDatabasesAPIView.as_view(),
        name='database_create'
    ),
    path(
        'tasks/',
        view=SQLTaskAPIView.as_view(),
        name='task_create'
    ),
    path(
        'tests/',
        view=TestsSQLAPIView.as_view(),
        name='test_create'
    ),
    path(
        'tasks/check/',
        view=CheckSQLQueryResponseAPIView.as_view(),
        name='task_check'
    ),
    path(
        'profile/tests/',
        view=UserTestAPIView.as_view(),
        name='user_tests'
    ),
    path(
        'profile/tests/<int:id>/tasks/',
        view=SQLTaskDetailAPIView.as_view(),
        name='user_tests'
    )
]

'''
    path(
        'tests/',
        view=None,#.as_view(),
        name='test_list'
    ),
    path(
        'task/',
        view=None,
        name='task_list'
    ),
    path(
        'task/check/',
        view=None,
        name='task_check'
    ),
    '''