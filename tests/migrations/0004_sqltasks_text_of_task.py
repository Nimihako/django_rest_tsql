# Generated by Django 2.2.11 on 2020-05-26 18:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0003_sqltest_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='sqltasks',
            name='text_of_task',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]
