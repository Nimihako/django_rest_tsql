import requests
from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy as _
from django.utils.duration import _get_duration_components
from rest_framework import serializers
from rest_framework.exceptions import (APIException, PermissionDenied,
                                       ValidationError)
from rest_framework.validators import UniqueValidator

from .models import TestsSQLDatabases, SQLTasks, SQLTest


class TestsSerializer(serializers.ModelSerializer):

    class Meta:
        model = SQLTest
        fields = ('id','name',)


class TestsSQLDatabasesSerializer(serializers.ModelSerializer):

    class Meta:
        model = TestsSQLDatabases
        fields = ('id', 'name',)

    def validate_name(self, name):
        if TestsSQLDatabases.objects.filter(name=name).exists():
            raise ValidationError(_('База данных с таким именем уже существует'))
        return name


class SQLTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = SQLTasks
        fields = (
            'id',
            'sql_query',
            'text_of_task',
            'databases'
        )


class SQLTestSerializer(serializers.ModelSerializer):

    class Meta:
        model = SQLTest
        fields = (
            'id',
            'tasks',
            'name',
            'users_groups',
            'date_work_start',
            'date_work_end'
        )

class CheckSQLQueryResponseSerializer(serializers.Serializer):
    #number_of_sql_task = serializers.IntegerField()
    number_of_sql_task = serializers.PrimaryKeyRelatedField(queryset=SQLTasks.objects.all(), write_only=True, required=False)
    user_sql_row = serializers.CharField()