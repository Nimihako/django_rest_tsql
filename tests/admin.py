from django.contrib import admin
from .models import SQLTasks, SQLTest, TestsSQLDatabases
# Register your models here.

admin.site.register(SQLTest)
admin.site.register(SQLTasks)
admin.site.register(TestsSQLDatabases)

